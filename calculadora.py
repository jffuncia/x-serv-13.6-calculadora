#!/usr/bin/env python3

import sys
import operator

if len(sys.argv) != 4:
    sys.exit("Numero argumentos incorrecto")

try:
    sys.argv[2] = float(sys.argv[2])
    sys.argv[3] = float(sys.argv[3])
except ValueError:
    sys.exit("numeros incorrectos")

Operation = sys.argv[1]
Value1 = sys.argv[2]
Value2 = sys.argv[3]

Operaciones = {"suma": operator.add, "resta": operator.sub, "multiplicacion": operator.mul, "division": operator.truediv}

try:
    Operacion in Operaciones
except:
    sys.exit("operacion invalida")

try:
    Operaciones[Operation](Value1, Value2)
except ZeroDivisionError:
    sys.exit("Operacion invalida. Divides por 0")

print(Operaciones[Operation](Value1, Value2))
